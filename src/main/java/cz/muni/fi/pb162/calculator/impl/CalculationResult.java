package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Result;

/**
 * Created by tomaspecuch on 31/10/15.
 * @author tomaspecuch
 */
public class CalculationResult implements Result {

    private double numericValue;
    private String alphanumericValue;
    private boolean succesful;
    private boolean isNumeric;

    /**
     * Constructor, create result using given param.
     * @param numericValue numeric value
     * @param alphanumericValue alphanumeric value, in case of error
     * @param succesful true if operation was succesful
     * @param isNumeric true if result is numeric
     */
    public CalculationResult(double numericValue, String alphanumericValue, boolean succesful, boolean isNumeric) {
        this.numericValue = numericValue;
        this.alphanumericValue = alphanumericValue;
        this.succesful = succesful;
        this.isNumeric = isNumeric;
    }
    @Override
    public boolean isSuccess() {
        return succesful;
    }

    @Override
    public boolean isAlphanumeric() {
        return !isNumeric;
    }

    @Override
    public boolean isNumeric() {
        return isNumeric;
    }

    @Override
    public double getNumericValue() {
        return numericValue;
    }

    @Override
    public String getAlphanumericValue() {
    return alphanumericValue;
    }
}
