package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Result;

/**
 * Created by tomaspecuch on 31/10/15.
 * @author tomaspecuch
 */
public class AdvancedCalculator extends BasicCalculator implements ConvertingCalculator {

    @Override
    public Result toDec(int base, String number) {
        if (base > 16 || base < 2) {
            return new CalculationResult(Double.NaN, COMPUTATION_ERROR_MSG, false, false);
        }

        boolean okString = true;
        for (int j = 0; j < number.length(); j++) {
            if (DIGITS.substring(0, base).indexOf(number.charAt(j)) < 0) {
                okString = false;
            }
        }
        if (!okString) {
            return new CalculationResult(Double.NaN, COMPUTATION_ERROR_MSG, false, false);
        }

        int v = 0;
        int total = 0;
        int pow = 0;
        //number = number.toUpperCase();
        for (int i = number.length() - 1; i >= 0; i--) {
            char c = number.charAt(i);
            if (c >= '0' && c <= '9') {
                v = c - '0';
            } else {
                if (c >= 'A' && c <= 'Z') {
                v = 10 + (c - 'A');
                }
            }
            total += v * Math.pow(base, pow);
            pow++;
        }
        return new CalculationResult(total, null, true, true);
    }

    @Override
    public Result fromDec(int base, int number) {
        if (base > 16 || base < 2) {
            return new CalculationResult(Double.NaN, COMPUTATION_ERROR_MSG, false, false);
        }
        String calcResult = Integer.toString(number, base);

        return new CalculationResult(Double.NaN, calcResult.toUpperCase(), true, false);
    }

    private Result calculate(String[] parts) {

        if (parts.length != 3) {
            return new CalculationResult(Double.NaN, WRONG_ARGUMENTS_ERROR_MSG, false, false);
        }

        switch (parts[0]) {
            case TO_DEC_CMD:
                return toDec(Integer.valueOf(parts[1]), parts[2]);
            default:
                return fromDec(Integer.valueOf(parts[1]), Integer.valueOf(parts[2]));
        }
    }


    @Override
    public Result eval(String input) {
        Result result = super.eval(input);
        if (!result.isSuccess()) {
            String[] parts = input.split(" ");
            switch (parts[0]) {
                case TO_DEC_CMD:
                case FROM_DEC_CMD:
                     return calculate(parts);
                default:
                    return new CalculationResult(Double.NaN, UNKNOWN_OPERATION_ERROR_MSG, false, false);
            }
        }

        return result;
    }

}