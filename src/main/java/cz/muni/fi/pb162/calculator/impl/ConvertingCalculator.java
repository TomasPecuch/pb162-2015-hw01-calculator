package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.NumeralConverter;

/**
 * Created by tomaspecuch on 04/11/15.
 * @author tomaspecuch
 */
public interface ConvertingCalculator extends Calculator, NumeralConverter {

}
