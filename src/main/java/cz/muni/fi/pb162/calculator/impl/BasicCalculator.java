package cz.muni.fi.pb162.calculator.impl;

import cz.muni.fi.pb162.calculator.Calculator;
import cz.muni.fi.pb162.calculator.Result;


/**
 * Created by tomaspecuch on 31/10/15.
 * @author tomaspecuch
 */
public class BasicCalculator implements Calculator{

    @Override
    public Result eval(String input) {
        String[] parts = input.split(" ");

        switch (parts[0]) {
            case SUM_CMD:
            case SUB_CMD:
            case MUL_CMD:
            case DIV_CMD:
                return calculate(parts);
            case FAC_CMD:
                return calculateFact(parts);
            default:
                return new CalculationResult(Double.NaN, UNKNOWN_OPERATION_ERROR_MSG, false, false);

        }
    }

    private Result calculateFact(String[] parts) {

        if (parts.length == 2) {
            int firstNumber = Integer.valueOf(parts[1]);
            return fac(firstNumber);
        } else {
            return new CalculationResult(Double.NaN, WRONG_ARGUMENTS_ERROR_MSG, false, false);
        }
    }

    private Result calculate(String[] parts) {

        if (parts.length != 3) {
            return new CalculationResult(Double.NaN, WRONG_ARGUMENTS_ERROR_MSG, false, false);
        }


        double firstNumber = Double.valueOf(parts[1]);
        double secondNumber = Double.valueOf(parts[2]);

        switch (parts[0]) {
            case SUM_CMD:
                return sum(firstNumber, secondNumber);
            case SUB_CMD:
                return sub(firstNumber, secondNumber);
            case MUL_CMD:
                return mul(firstNumber, secondNumber);
            case DIV_CMD:
                return div(firstNumber, secondNumber);
            default:
                return new CalculationResult(Double.NaN, UNKNOWN_OPERATION_ERROR_MSG, false, false);
        }
    }
    @Override
    public Result sum(double x, double y) {
       return new CalculationResult(x + y,null, true, true);
    }

    @Override
    public Result sub(double x, double y) {
        return new CalculationResult(x - y, null, true, true);

    }

    @Override
    public Result mul(double x, double y) {
        return new CalculationResult(x * y, null, true, true);
    }

    @Override
    public Result div(double x, double y) {
        if (y == 0) {
            return new CalculationResult(Double.NaN, COMPUTATION_ERROR_MSG, false, false);

        } else {
            return new CalculationResult(x / y, null, true, true);
        }
    }


    @Override
    public Result fac(int x) {
        if (x < 0) {
            return new CalculationResult(Double.NaN, COMPUTATION_ERROR_MSG, false, false);
        }

        double factResult = 1;

        if (x != 0) {
            for (int i = 1; i <= x; i++) {
                factResult = factResult * i;
            }
        }

        return new CalculationResult(factResult, null, true, true);
    }

}
